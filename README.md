# Bac Français

Texte et explication pour réviser le Bac de français

## Fonctionnement

Le principe de ce Git Lab est d'accumuler différentes correction possibles d'explication linéaire pour le bac francais 2021-2022.
hacun peux ajouter sa propre version d'explication d'un texte, l'objectif étant d'éviter d'avoir à faire soi-mêmes chaque textes, vous mettez donc votre travail à disposition des autres en échange de pouvoir exploiter le leur. N'hésité pas à ajouter votre explication même si une version du même texte est déjà disponible : Chacun pourra ensuite choisir celle qu'elle préfère. Si vous voulez modifier un document, copier le !! Ne detruisez pas directement le travail d'un autre.  

## Ajout d'un document

Si possible rédigez votre document selon le modèle proposé dans le dossier Modèles & Doc, afin d'avoir un ensemble final de fichier rédigé et construit de la même façon. Ajouter votre Explication dans le Dossier explication linéaire avec les autres version. Pour l'instant on ajoute en bordel si y'en a trop après on organisera plus proprement
Votre fichier doit avoir le noms suivant : **Texte_étudié/Nom_Prénom.pdf** et être au format PDF svp merci

## Fiche de Révision

Possible ajout futur d'un dossier Fiche de Révision contenant des notes pour les textes et la question de grammaire

## Futur du Git

Ce Git aura possiblement d'autre fonction plus tard ( sujet de contrôle, fiche révision par matière, etc... )

bises sur tes fesses cousin ;)

